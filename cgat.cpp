#include<algorithm>
#include<iostream>
#include<fstream>
#include<sstream>
#include<cstring>
#include<string>
#include<ctime>
#include<list>
#include<cstdio>
#include<cmath>

using namespace std;
long randlong(long ubound);
long get_seed(void);
void usage(long, char**);
list<string> shuffle_sequence(string, long);
string save_sequences(list<string>, string);

class Shufresults {
	long shuffles;
	double mean, stdev, zscore;
	string unshuff;
	//public:
		/*void set_scores(list<double> scores) {
			double totalscore = 0;
			long n = 0;
			for (list<double>::iterator score=scores.begin(); score != scores.end(); score++) {
				totalscore += score;
			}
		}*/
};
Shufresults run_needle(string, string, long, long, long);

long randlong(long ubound) {
	return rand() % ubound;
}
long get_seed() {
	return time(0) + clock();
}
void usage(long argc, char **argv) {
	cout << "Usage: " << argv[0] << " ASEQUENCE BSEQUENCE SHUFFLES\n";
}

list<string> shuffle_sequence(string sequence, long shuffles = 500) {
	//cout << sequence << sequence.length() << endl;
	list<string> shuffled;

	//the first sequence is the original
	string a_copy = string(sequence);
	shuffled.push_back(a_copy);
	for (long i = 0; i < shuffles; i++) {
		string a_copy = string(sequence);
		random_shuffle(a_copy.begin(), a_copy.end(), randlong);
		shuffled.push_back(a_copy);
		//cout << a_copy << endl;
	}
	/*a_copy  = string(sequence);
	random_shuffle(a_copy.begin(), a_copy.end(), randlong);
	cout << "shuffled\n";
	cout << sequence << endl;
	cout << a_copy << endl;
	cout << "shuffled again\n";
	random_shuffle(a_copy.begin(), a_copy.end(), randlong);
	cout << sequence << endl;
	cout << a_copy << endl;*/
	return shuffled;
}

string save_sequences(list<string> seqlist, string prefix = "") {
	char rawfn[L_tmpnam];
	char* polonger;
	tmpnam(rawfn);

	fstream f;
	f.open(rawfn, fstream::out);
	long n = 0;
	for (list<string>::iterator i=seqlist.begin(); i != seqlist.end(); i++) {
		f << ">seq_" << prefix << n << endl;
		//cout << ">seq_" << prefix << n << endl;
		f << *i << endl;
		//cout << *i << endl;
		n++;
	}

	string fn;
	fn = rawfn;
	return fn;
}

Shufresults run_needle(string afn, string bfn, long shuffles, long gapopen, long gapextend) {
	char rawfn[L_tmpnam];
	char* polonger;
	tmpnam(rawfn);

	char* rawcmd;
	string cmd;
	cmd += "needle " + afn + " " + bfn + " -gapopen " + to_string(gapopen) + " -gapextend " + to_string(gapextend) + " -outfile " + rawfn;
	//cout << cmd << endl;
	system(cmd.c_str());

	ifstream is(rawfn);
	string l;

	list<double> scores;
	double totalscore = 0;

	bool header = false;

	long aln = 0;
	double unshufscore = -999.;

	string unshuftext = "";

	while (getline(is, l)) {
		//cout << l[0] << endl;
		if (!strncmp(l.c_str(), "#==", 3)) {
			header = !header;
			if (header) {
				aln += 1;
			}
		}
		else if (header) {
			if (!strncmp(l.c_str(), "# Score: ", 9)) {
				string tok;
				istringstream iss(l);
				int t = 0;
				while (getline(iss, tok, ' ')) {
					if (t == 2) {
						if (aln <= 1) { 
							unshufscore = stod(tok);
						} else {
							scores.push_back(stod(tok));
							totalscore += stod(tok);
							//cout << tok << endl;
						}
					}
					t++;
				}
				//scores.push_back(
			}
		}
		if (aln <= 1) {
			unshuftext += l + "\n";
		}
	}
	double mean = totalscore / aln;
	double deviation = 0;
	for (list<double>::iterator s=scores.begin(); s != scores.end(); s++) {
		deviation += (*s - mean) * (*s - mean);
		//cout << *s << endl;
	}
	deviation /= (shuffles - 1);
	double stdeviation = sqrt(deviation);
	double zscore = (unshufscore - mean) / stdeviation;
	/*cout << "unshuf: " << unshufscore << endl;
	cout << "mean:   " << mean << endl;
	cout << "stdev:  " << stdeviation << endl;
	cout << "shuff:  " << shuffles - 1 << endl;
	cout << "zscore: " << zscore << endl; */

	cout << unshuftext << endl;
	printf("============ FINISHED =============\n");
	printf("Average Quality (AQ)\t%0.2f +/- %0.2f\n", mean, stdeviation);
	printf("Standard score (Z):\t%0.1f\n", zscore);
	printf("Precise score (Z):\t%0.3f\n", zscore);
	printf("Shuffles:\t%d\n\n", shuffles);

	//maybe later
	Shufresults results;

	return results;

}

int main(int argc, char **argv) {
	string str;
	const long seed = get_seed();
	srand(seed);
	/*cout << "Please enter a word: ";
	//cin >> str;
	str = "abcdefgh";
	random_shuffle(str.begin(), str.end(), randlong);
	cout << str << '\n';*/

	string asequence = "";
	string bsequence = "";
	long gapopen = 8;
	long gapextend = 2;
	long shuffles = 500;
	bool verbose = false;
	string outfile = "-";
	double zscore = -1;
	double gaps = -1;
	double average = 0;

	if (argc < 3) {
		usage(argc, argv);
		return 1;
	} else if (argc >= 3) {
		shuffles = atof(argv[3]);
	}
	list<string> ashuf = shuffle_sequence(argv[1], 0);
	list<string> bshuf = shuffle_sequence(argv[2], shuffles);
	string afn = save_sequences(ashuf, "A");
	string bfn = save_sequences(bshuf, "B");

	Shufresults results = run_needle(afn, bfn, shuffles, gapopen, gapextend);

	/*cout << afn << endl;
	cout << bfn << endl;
	string test;
	cin >> test;*/

	if (remove(bfn.c_str()) != 0) {
		cout << "Could not remove file '" << bfn << "'"<< endl;
	}
	if (remove(afn.c_str()) != 0) {
		cout << "Could not remove file '" << bfn << "'"<< endl;
	}

	return 0;
}
